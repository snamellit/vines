use yew::prelude::*;
use yew::virtual_dom::AttrValue;

mod row {
    #[derive(Debug)]
    pub struct Row {
        index: u32,
        offset_first_row: f32,
        offset_first_pole: f32, // offset start pole from baseline in meters
        length: f32,            // distance between start and end pole
    }

    const ROW_DISTANCE: f32 = 1.5;
    const ROW_OFFSET: f32 = 1.0;
    const ROW_LENGTH: f32 = 52.0;

    /// Create a new row using default dimensions
    ///
    /// Simple constructor creating rows using default dimensions for
    /// distances
    ///
    /// ```
    /// let row1 = create(1);
    /// let row2 = create(2);
    /// let row3 = create(3);
    /// assert_eq!(row1.index, 1)
    /// assert_eq!(row2.index, 2)
    /// assert_eq!(row3.index, 3)
    /// assert_eq!(row1.offset_first_row, 0.0f32)
    /// assert_eq!(row2.offset_first_row, 1.5f32)
    /// assert_eq!(row3.offset_first_row, 3.0f32)
    /// ```

    pub fn create(index: u32) -> Row {
        Row {
            index,
            offset_first_row: ((index - 1) as f32) * ROW_DISTANCE,
            offset_first_pole: ROW_OFFSET * ((index - 1) % 2) as f32,
            length: ROW_LENGTH,
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn test_create() {
            let row1 = create(1);
            let row2 = create(2);
            let row3 = create(3);
            assert_eq!(row1.index, 1);
            assert_eq!(row2.index, 2);
            assert_eq!(row3.index, 3);
            assert_eq!(row1.offset_first_row, 0.0f32);
            assert_eq!(row2.offset_first_row, 1.5f32);
            assert_eq!(row3.offset_first_row, 3.0f32);
            assert_eq!(row1.offset_first_pole, 0.0f32);
            assert_eq!(row2.offset_first_pole, 1.0f32);
            assert_eq!(row3.offset_first_pole, 0.0f32);
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct RowProps {
    index: AttrValue,
}

impl Component for row::Row {
    type Message = ();
    type Properties = RowProps;

    fn create(ctx: &Context<Self>) -> Self {
        let index = ctx.props().index.to_string();
        let index = index.parse::<u32>().unwrap_or(0);
        row::create(index)
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        // This gives the component 'Scope' to send messages et al...
        html!(
            <div>
                <p>{format!("{self:?}")}</p>
            </div>
        )
    }
}

enum Msg {
    AddOne,
    SubOne,
}

struct Model {
    value: i64,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self { value: 0 }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::AddOne => {
                self.value += 1;
                // state has changes so we must re-render
                true
            }
            Msg::SubOne => {
                self.value -= 1;
                // state has changes so we must re-render
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        // This gives the component 'Scope' to send messages et al...
        let link = ctx.link();
        html!(
            <div>
                <button onclick={link.callback(|_| Msg::SubOne)}>{"-1"}</button>
                <button onclick={link.callback(|_| Msg::AddOne)}>{"+1"}</button>
                <p>{self.value}</p>
            {
                (1..=14).into_iter().map(|i| {
                                        html!{<row::Row index={i.to_string()}/>}

                }).collect::<Html>()
            }
            </div>
        )
    }
}

fn main() {
    yew::start_app::<Model>();
}
